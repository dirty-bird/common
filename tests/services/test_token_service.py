import pytest
import jwt
from datetime import datetime, timedelta
from dateutil import parser

SECRET = '0vi38vPuE8'

from datetime import datetime, timedelta

from dirtybird.services import TokenService


@pytest.fixture()
def token_service():
    return TokenService(SECRET)


def test_encode_no_kwargs(token_service):
    with pytest.raises(Exception):
        token_service.encode()


def test_encode(token_service):
    token = token_service.encode(test_1=123, test_2=456)
    decoded = jwt.decode(token, SECRET)
    assert decoded.get('__ed')
    assert parser.parse(decoded['__ed']) > datetime.utcnow()

    assert decoded.get('test_1')
    assert decoded['test_1'] == 123
    assert decoded.get('test_2')
    assert decoded['test_2'] == 456

    expiration_date = datetime.utcnow() + timedelta(minutes=30)
    token = token_service.encode(test_1=123, test_2=456, expiration_date=expiration_date)
    decoded = jwt.decode(token, SECRET)
    assert decoded.get('__ed')
    assert parser.parse(decoded['__ed']) == expiration_date

    assert decoded.get('test_1')
    assert decoded['test_1'] == 123
    assert decoded.get('test_2')
    assert decoded['test_2'] == 456

    token = token_service.encode(test_1=123, test_2=456, never_expires=True)
    decoded = jwt.decode(token, SECRET)
    assert not decoded.get('__ed')

    assert decoded.get('test_1')
    assert decoded['test_1'] == 123
    assert decoded.get('test_2')
    assert decoded['test_2'] == 456

    token = token_service.encode(test_1=123, test_2=456, expiration_date=expiration_date, never_expires=True)
    decoded = jwt.decode(token, SECRET)
    assert not decoded.get('__ed')

    assert decoded.get('test_1')
    assert decoded['test_1'] == 123
    assert decoded.get('test_2')
    assert decoded['test_2'] == 456


def test_decode(token_service):
    token = jwt.encode({'test_1': 123, 'test_2': 456}, SECRET)
    data = token_service.decode(token)
    assert data.get('test_1')
    assert data['test_1'] == 123
    assert data.get('test_2')
    assert data['test_2'] == 456
    assert not data.get('__ed')

    expiration_date = datetime.utcnow() + timedelta(minutes=60)
    token = jwt.encode({'test_1': 123, 'test_2': 456, '__ed': str(expiration_date)}, SECRET)
    data = token_service.decode(token)
    assert data.get('test_1')
    assert data['test_1'] == 123
    assert data.get('test_2')
    assert data['test_2'] == 456
    assert not data.get('__ed')

    expiration_date = datetime.utcnow() - timedelta(minutes=60)
    token = jwt.encode({'test_1': 123, 'test_2': 456, '__ed': str(expiration_date)}, SECRET)
    data = token_service.decode(token)
    assert not data
