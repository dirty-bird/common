import asynctest
import bcrypt
import pytest

from motor.motor_asyncio import AsyncIOMotorClient as MotorClient


@pytest.fixture()
def host():
    return 'localhost:10010'


@pytest.fixture()
def admin_username():
    return 'adm1n'


@pytest.fixture()
def admin_password():
    return 'd1rtyb1rd'


@pytest.fixture()
def database_name():
    return 'dirtybird-test'


@pytest.fixture()
def mongo_client(event_loop, admin_username, admin_password, host):
    class MongoClient:

        def __init__(self, event_loop, admin_username, admin_password, host):
            self.loop = event_loop
            self.mongo_client = MotorClient('mongodb://{}:{}@{}'.format(admin_username, admin_password, host), io_loop=event_loop)

        async def drop_database(self, database_name):
            await self.mongo_client.drop_database(database_name)

        async def create_database(self, host, name):
            username, password = 'admin', 'password'
            database = self.mongo_client[name]
            await database.add_user(username, password)
            uri = 'mongodb://{}:{}@{}/{}'.format(username, password, host, name)

            database_client = MotorClient(uri, io_loop=self.loop)
            database = database_client.get_default_database()

            return database, host, name, username, password

    return MongoClient(event_loop, admin_username, admin_password, host)


@pytest.fixture()
async def test_mongo(request, mongo_client, event_loop, database_name, host):
    class MongoDatabase:

        def __init__(self, database, host, name, username, password):
            self.database = database
            self.host = host
            self.database_name = database_name
            self.username = username
            self.password = password

    args = await mongo_client.create_database(host, database_name)
    db = MongoDatabase(*args)

    def fin():
        async def afin():
            await mongo_client.drop_database(database_name)
        event_loop.run_until_complete(afin())

    request.addfinalizer(fin)
    return db
