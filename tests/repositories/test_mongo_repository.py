import pytest
import uuid

from dirtybird.errors import NotInstance, NotType
from dirtybird.models import Entity, AuditableEntity
from dirtybird.models.types import StringType, IntType
from dirtybird.repositories import MongoRepository
from dirtybird.utils import get_mongo_database


COLLECTION_NAME = 'test'


class FakeModel(AuditableEntity):
    first_name = StringType(required=True)
    last_name = StringType(required=True)
    age = IntType(required=True)

    def __init__(self, first_name=None, last_name=None, age=None, _id=None, **kwargs):
        super(FakeModel, self).__init__(_id=_id, first_name=first_name, last_name=last_name, age=age)

    def equals(self, fake_model):
        return isinstance(fake_model, FakeModel) \
            and self._id == fake_model._id \
            and self.first_name == fake_model.first_name \
            and self.last_name == fake_model.last_name \
            and self.age == fake_model.age


ID = uuid.uuid4()
AGE = 23
AGE_2 = 21
JANE = 'Jane'
JOHN = 'John'
SMITH = 'Smith'


@pytest.fixture()
def jane():
    return FakeModel(JANE, SMITH, AGE, ID)


@pytest.fixture()
def john():
    return FakeModel(JOHN, SMITH, 26)


@pytest.fixture()
def mary():
    return FakeModel('Mary', 'Johnson', AGE_2)


@pytest.fixture()
def bob():
    return FakeModel('Bob', 'Williams', AGE)


@pytest.fixture()
def jane2():
    return FakeModel(JANE, 'Miller', 24)


@pytest.fixture()
def base_repository(event_loop, test_mongo):
    database = get_mongo_database(event_loop, test_mongo.host, test_mongo.database_name, test_mongo.username, test_mongo.password)
    return MongoRepository(database, FakeModel, COLLECTION_NAME)


@pytest.fixture()
async def delete_data_in_collection(request, event_loop, test_mongo):
    def fin():
        async def afin():
            await test_mongo.database[COLLECTION_NAME].drop()
        event_loop.run_until_complete(afin())
    request.addfinalizer(fin)


@pytest.fixture()
@pytest.mark.usefixtures("delete_data_in_collection")
async def with_test_data(test_mongo, jane, john, mary, bob, jane2):
    documents = [person.to_primitive() for person in [jane, john, mary, bob, jane2]]
    await test_mongo.database[COLLECTION_NAME].insert_many(documents)


@pytest.mark.asyncio
@pytest.mark.usefixtures("with_test_data")
async def test_get(base_repository, test_mongo, mary, john, jane):
    return_person = await base_repository.get(first_name=JOHN)
    assert john.equals(return_person)

    return_person = await base_repository.get(age=AGE_2)
    assert mary.equals(return_person)

    return_person = await base_repository.get(first_name=JANE, last_name=SMITH)
    assert jane.equals(return_person)


@pytest.mark.asyncio
async def test_get_by_id_not_uuid(base_repository):
    with pytest.raises(NotType):
        await base_repository.get_by_id({})

    with pytest.raises(NotType):
        await base_repository.get_by_id('test')

    with pytest.raises(NotType):
        await base_repository.get_by_id(123123)


@pytest.mark.asyncio
@pytest.mark.usefixtures("with_test_data")
async def test_get_by_id(base_repository, test_mongo, jane):
    return_person = await base_repository.get_by_id(ID)
    return_person = await base_repository.get(first_name=JANE, last_name=SMITH)
    assert jane.equals(return_person)


@pytest.mark.asyncio
@pytest.mark.usefixtures("with_test_data")
async def test_list(base_repository, test_mongo, jane, jane2, john, bob, mary):

    people = await base_repository.list()
    assert isinstance(people, list)
    assert len(people) == 5
    assert any(jane.equals(person) for person in people)
    assert any(jane2.equals(person) for person in people)
    assert any(john.equals(person) for person in people)
    assert any(mary.equals(person) for person in people)
    assert any(bob.equals(person) for person in people)

    people = await base_repository.list(last_name=SMITH)
    assert isinstance(people, list)
    assert len(people) == 2
    assert any(jane.equals(person) for person in people)
    assert any(john.equals(person) for person in people)

    people = await base_repository.list(age=AGE)
    assert isinstance(people, list)
    assert len(people) == 2
    assert any(jane.equals(person) for person in people)
    assert any(bob.equals(person) for person in people)


@pytest.mark.asyncio
@pytest.mark.usefixtures("with_test_data")
async def test_exists(base_repository, test_mongo, mary, john, jane):
    assert await base_repository.exists(first_name=JOHN)
    assert await base_repository.exists(age=AGE_2)
    assert await base_repository.exists(first_name=JANE, last_name=SMITH)

    jones = 'Jones'
    assert not await base_repository.exists(last_name=jones)
    assert not await base_repository.exists(first_name=JANE, last_name=jones)


@pytest.mark.asyncio
async def test_exists_by_id_not_uuid(base_repository):
    with pytest.raises(NotType):
        await base_repository.exists_by_id({})

    with pytest.raises(NotType):
        await base_repository.exists_by_id('test')

    with pytest.raises(NotType):
        await base_repository.exists_by_id(123123)


@pytest.mark.asyncio
@pytest.mark.usefixtures("with_test_data")
async def test_exists_by_id(base_repository, test_mongo, jane):
    return_person = await base_repository.get_by_id(ID)
    return_person = await base_repository.get(first_name=JANE, last_name=SMITH)
    assert jane.equals(return_person)


@pytest.mark.asyncio
async def test_save_not_base_model(base_repository):
    with pytest.raises(NotInstance):
        await base_repository.save({})

    with pytest.raises(NotInstance):
        await base_repository.save('test')

    with pytest.raises(NotInstance):
        await base_repository.save(123123)

    class A:
        pass

    with pytest.raises(NotInstance):
        await base_repository.save(A())


@pytest.mark.asyncio
async def test_save_not_specified_model_type(base_repository):
    class A(Entity):
        pass

    with pytest.raises(NotType):
        await base_repository.save(A(id=uuid.uuid4()))


@pytest.mark.asyncio
@pytest.mark.usefixtures("delete_data_in_collection")
async def test_save_new(base_repository, test_mongo, jane):
    await base_repository.save(jane)
    document = await test_mongo.database[COLLECTION_NAME].find_one({'_id': str(ID)})
    assert document is not None
    assert document['_id'] == str(ID)
    assert document['first_name'] == JANE
    assert document['last_name'] == SMITH
    assert document['age'] == AGE


@pytest.mark.asyncio
@pytest.mark.usefixtures("with_test_data")
async def test_save_exisiting(base_repository, test_mongo, jane):
    document = await test_mongo.database[COLLECTION_NAME].find_one({'_id': str(ID)})

    assert document is not None
    assert document['_id'] == str(ID)
    assert document['first_name'] == JANE
    assert document['last_name'] == SMITH
    assert document['age'] == AGE

    janey = 'Janey'
    baker = 'Baker'

    jane.first_name = janey
    await base_repository.save(jane)
    document = await test_mongo.database[COLLECTION_NAME].find_one({'_id': str(ID)})
    assert document is not None
    assert document['first_name'] == janey

    jane.last_name = baker
    await base_repository.save(jane)
    document = await test_mongo.database[COLLECTION_NAME].find_one({'_id': str(ID)})
    assert document is not None
    assert document['last_name'] == baker

    jane.age = jane.age + 1
    await base_repository.save(jane)
    document = await test_mongo.database[COLLECTION_NAME].find_one({'_id': str(ID)})
    assert document is not None
    assert document['age'] == AGE + 1


@pytest.mark.asyncio
@pytest.mark.usefixtures("with_test_data")
async def test_delete(base_repository, test_mongo, mary, john, jane):
    assert await test_mongo.database[COLLECTION_NAME].count() == 5

    await base_repository.delete(first_name=JOHN)
    assert not await test_mongo.database[COLLECTION_NAME].find_one({'first_name': JOHN})
    assert await test_mongo.database[COLLECTION_NAME].count() == 4

    await base_repository.delete(age=AGE_2)
    assert not await test_mongo.database[COLLECTION_NAME].find_one({'age': AGE_2})
    assert await test_mongo.database[COLLECTION_NAME].count() == 3

    await base_repository.delete(first_name=JANE, last_name=SMITH)
    assert not await test_mongo.database[COLLECTION_NAME].find_one({'first_name': JANE, 'last_name': SMITH})
    assert await test_mongo.database[COLLECTION_NAME].count() == 2


@pytest.mark.asyncio
async def test_delete_by_id_not_uuid(base_repository):
    with pytest.raises(NotType):
        await base_repository.delete_by_id({})

    with pytest.raises(NotType):
        await base_repository.delete_by_id('test')

    with pytest.raises(NotType):
        await base_repository.delete_by_id(123123)


@pytest.mark.asyncio
@pytest.mark.usefixtures("with_test_data")
async def test_delete_by_id(base_repository, test_mongo, jane):
    assert await test_mongo.database[COLLECTION_NAME].count() == 5

    await base_repository.delete_by_id(ID)
    assert not await test_mongo.database[COLLECTION_NAME].find_one({'_id': str(ID)})
    assert await test_mongo.database[COLLECTION_NAME].count() == 4


@pytest.mark.asyncio
@pytest.mark.usefixtures("with_test_data")
async def test_count(base_repository, test_mongo, jane):
    assert await base_repository.count() == 5
    assert await base_repository.count(last_name=SMITH) == 2
    assert await base_repository.count(age=AGE) == 2
