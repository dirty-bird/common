import pytest
from datetime import datetime

from dirtybird.models import AuditableEntity


def test_modified():
    auditable_entity = AuditableEntity()

    auditable_entity.modified()
    assert isinstance(auditable_entity.modified_at, datetime)
    assert auditable_entity.modified_by == AuditableEntity.DEFAULT_BY

    test_by = 'test'
    auditable_entity.modified(test_by)
    assert isinstance(auditable_entity.modified_at, datetime)
    assert auditable_entity.modified_by == test_by
