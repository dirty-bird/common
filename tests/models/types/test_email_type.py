import pytest
from schematics.exceptions import ValidationError
from dirtybird.models.types import EmailType


def test_validate():
    with pytest.raises(ValidationError):
        EmailType().validate(1)

    with pytest.raises(ValidationError):
        EmailType().validate('test')

    assert EmailType().validate('test@test.com')
