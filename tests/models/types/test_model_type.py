import pytest
import uuid

from schematics.exceptions import ValidationError
from dirtybird.models import BaseModel
from dirtybird.models.types import ModelType, UUIDType, StringType


class Model(BaseModel):
    _id = UUIDType(required=True, serialized_name='id')
    name = StringType(required=True)

    def __init__(self, id, name):
        super(Model, self).__init__(_id=id, name=name)

model_type = ModelType()
model_type_with_class = ModelType(Model)


def test_to_native():
    value = {'id': uuid.uuid4(), 'name': 'test'}
    assert type(model_type.to_native(value)) is BaseModel
    assert type(model_type_with_class.to_native(value)) is Model


def test_validate():
    base_model = BaseModel()
    model = Model(uuid.uuid4(), 'test')

    with pytest.raises(ValidationError):
        model_type.validate(1)

    with pytest.raises(ValidationError):
        model_type.validate('test')

    with pytest.raises(ValidationError):
        model_type.validate([])

    model_type.validate({})
    model_type.validate(base_model)
    model_type.validate(model)

    with pytest.raises(TypeError):
        model_type_with_class.validate({})

    with pytest.raises(ValidationError):
        model_type_with_class.validate(base_model)

    model_type_with_class.validate(model)
