import pytest
import uuid

from dirtybird.models import BaseModel, ModelList
from dirtybird.models.types import UUIDType, StringType


class Model(BaseModel):
    id = UUIDType(required=True)
    name = StringType(required=True)

    def __init__(self, id, name):
        super(Model, self).__init__(name=name, id=id)


def test_model_list():
    model_1_id = uuid.uuid4()
    model_2_id = uuid.uuid4()
    model_1_name = 'John'
    model_2_name = 'Steve'
    model_1 = Model(model_1_id, model_1_name)
    model_2 = Model(model_2_id, model_2_name)
    models = [model_1, model_2]
    domain_list = ModelList(models)

    assert len(domain_list.items) == 2
    assert isinstance(domain_list.items[0], Model)
    assert isinstance(domain_list.items[1], Model)

    primitive = domain_list.to_primitive()

    assert primitive[0] == model_1.to_primitive()
    assert primitive[1] == model_2.to_primitive()
