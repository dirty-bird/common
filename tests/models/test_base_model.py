import pytest
import uuid

from dirtybird.models import BaseModel
from dirtybird.models.types import UUIDType, StringType


class Model(BaseModel):
    _id = UUIDType(required=True, serialized_name='id')
    name = StringType(required=True)

    def __init__(self, id, name):
        super(Model, self).__init__(_id=id, name=name)


def test_get_serialed_name():
    with pytest.raises(ValueError):
        assert Model.get_serialized_name('test')

    assert Model.get_serialized_name('name') == 'name'

    assert Model.get_serialized_name('_id') == 'id'
