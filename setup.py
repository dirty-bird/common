from setuptools import find_packages, setup

install_requires = [
    'motor>=1.1',
    'schematics>=2.0.1',
    'asyncio>=3.4.3',
    'async_generator>=1.8',
    'python-dateutil>=2.6.1',
    'py-bcrypt>=0.4',
    'validate-email>=1.3'
]


tests_require = [
    'pytest>=3.2.0',
    'pytest-asyncio>=0.6.0',
    'asynctest',
]

setup(name='dirtybird-common',
      version='0.0.6',
      description='Dirty Bird Common',
      author='Joseph Kestel',
      author_email='josey@dirtybird.io',
      url='http://dirtybird.io',
      platforms=['any'],
      python_requires='~=3.5',
      license='Copyright (c) 2017 Jospeh Kestel. All Rights Reserved',
      packages=find_packages(),
      setup_requires=['pytest-runner'],
      install_requires=install_requires,
      tests_require=tests_require,
      test_suite='pytest'
      )
