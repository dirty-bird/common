from motor.motor_asyncio import AsyncIOMotorClient as MotorClient


def get_mongo_database(event_loop, host, database_name, username, password):
    url = 'mongodb://{}:{}@{}/{}'.format(username, password, host, database_name)
    return MotorClient(url, io_loop=event_loop).get_default_database()


def get_mongo_database_from_url(event_loop, url):
    return MotorClient(url, io_loop=event_loop).get_default_database()
