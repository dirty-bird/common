import bcrypt


def create_salt_and_hashed(raw):
    salt = bcrypt.gensalt()
    return salt, bcrypt.hashpw(raw, salt)


def is_hashed(raw, hashed):
    return bcrypt.hashpw(raw, hashed) == hashed
