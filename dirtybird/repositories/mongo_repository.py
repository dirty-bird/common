import asyncio
import uuid

from ..errors import NotInstance, NotType
from ..models import Entity, AuditableEntity


@asyncio.coroutine
def get(cursor, length=None):
    documents = yield from cursor.to_list(length)
    return documents


class MongoRepository:

    def __init__(self, database, entity_type, collection_name):
        self._entity_type = entity_type
        self._collection = database[collection_name]

    # PRIVATE/PROTECTED
    def _to_entity(self, document):
        return self._entity_type(**document) if document else None

    def _to_document(self, entity):
        return entity.to_primitive()

    def _serialize_id(self, id):
        if type(id) != uuid.UUID:
            raise NotType(id, uuid.UUID)

        return str(id)

    def _get_spec(self, spec):
        if spec is None:
            return None
        return {self._entity_type.get_serialized_name(key): value for key, value in spec.items()}

    # PUBLIC

    async def get(self, **spec):
        document = await self._collection.find_one(self._get_spec(spec))
        return self._to_entity(document)

    async def get_by_id(self, id):
        return await self.get(_id=self._serialize_id(id))

    async def list(self, length=None, **spec):
        cursor = self._collection.find(self._get_spec(spec))
        documents = await get(cursor, length)
        return [self._to_entity(document) for document in documents]

    async def exists(self, **spec):
        return await self.get(**spec) is not None

    async def exists_by_id(self, id):
        return await self.exists(_id=self._serialize_id(id))

    async def save(self, entity, updated_by=None):
        if not isinstance(entity, Entity):
            raise NotInstance(entity, Entity)

        if type(entity) != self._entity_type:
            raise NotType(entity, self._entity_type)

        if isinstance(entity, AuditableEntity):
            entity.modified(updated_by)

        document = self._to_document(entity)
        spec = {'_id': self._serialize_id(entity.id)}

        try:
            await self._collection.replace_one(spec, document, upsert=True)
        except Exception as e:
            raise e  # TODO: get more detailed about this (ex: connection error, validation error, etc)

        return entity

    async def delete(self, **spec):
        await self._collection.delete_many(self._get_spec(spec))

    async def delete_by_id(self, id):
        await self.delete(_id=self._serialize_id(id))

    async def count(self, **spec):
        spec = spec if len(spec.keys()) > 0 else None
        return await self._collection.count(self._get_spec(spec))
