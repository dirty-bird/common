class VaidationError(Exception):
    pass


class ConflictError(Exception):
    pass


class NotInstance(Exception):

    def __init__(self, instance, cls):
        super(NotInstance, self).__init__('{} is not an instance of \'{}\'.'.format(instance, cls.__name__))


class NotType(Exception):

    def __init__(self, instance, cls):
        super(NotType, self).__init__('{} is not of type \'{}\'.'.format(instance, cls.__name__))
