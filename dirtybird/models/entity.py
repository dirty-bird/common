import uuid

from .types import UUIDType
from .base_model import BaseModel


class Entity(BaseModel):

    _id = UUIDType(required=True)

    def __init__(self, _id=None, **kwargs):
        super(Entity, self).__init__(_id=uuid.uuid4() if not _id else _id, **kwargs)

    @property
    def id(self):
        return self._id
