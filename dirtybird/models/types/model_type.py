from schematics.exceptions import ValidationError

from ..base_model import BaseModel
from schematics.types import BaseType


class ModelType(BaseType):

    def __init__(self, model_cls=None, **kwargs):
        if model_cls is not None and not issubclass(model_cls, BaseModel):
            raise TypeError('{} must implement {}'.format(model_cls, BaseModel.__name__))
        self._model_cls = BaseModel if model_cls is None else model_cls
        super(ModelType, self).__init__(**kwargs)

    def to_native(self, value, context=None):
        return self._model_cls(**value) if isinstance(value, dict) else value

    def to_primitive(self, value, context=None):
        return value.to_primitive()

    def validate_model_type(self, value, context=None):
        if not isinstance(value, self._model_cls):
            raise ValidationError('Value must be an instance of {}'.format(self._model_cls.__name__))
        try:
            value.validate()
        except Exception as e:
            return False
        return True
