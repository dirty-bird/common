from schematics.types import ListType
from .model_type import ModelType


class ModelListType(ListType):

    def __init__(self, **kwargs):
        super(ModelListType, self).__init__(ModelType, **kwargs)

    def to_native(self, value, context=None):
        if isinstance(value, list):
            return value
        return list()
