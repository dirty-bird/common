from validate_email import validate_email

from schematics.exceptions import ValidationError
from schematics.types import StringType


class EmailType(StringType):

    def validate_email(self, value):
        if not validate_email(str(value)):
            raise ValidationError('Value must be an email address.')
