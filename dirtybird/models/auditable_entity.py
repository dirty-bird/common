from datetime import datetime
from .types import DateTimeType, StringType
from .entity import Entity


class AuditableEntity(Entity):

    DEFAULT_BY = 'system'

    _created_at = DateTimeType(required=True, serialized_name='created_at')
    _created_by = StringType(required=True, serialized_name='created_by')
    _modified_at = DateTimeType(serialized_name='modified_at')
    _modified_by = StringType(serialized_name='modified_by')

    def __init__(self, created_at=None, created_by=None, modified_at=None, modified_by=None, **kwargs):
        created_at = datetime.utcnow() if created_at is None else created_at
        created_by = AuditableEntity.DEFAULT_BY if created_by is None else created_by
        super(AuditableEntity, self).__init__(_created_at=created_at, _created_by=created_by, _modified_at=modified_at, _modified_by=modified_by, **kwargs)

    @property
    def created_at(self):
        return self._created_at

    @property
    def created_by(self):
        return self._created_by

    @property
    def modified_at(self):
        return self._modified_at

    @property
    def modified_by(self):
        return self._modified_by

    def modified(self, modified_by=None):
        self._modified_at, self._modified_by = datetime.utcnow(), AuditableEntity.DEFAULT_BY if modified_by is None else modified_by
