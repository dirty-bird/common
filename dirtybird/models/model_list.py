from .base_model import BaseModel
from .types import ModelListType


class ModelList(BaseModel):
    items = ModelListType(required=True)

    def __init__(self, items=None, **kwargs):
        super(ModelList, self).__init__(items=items, **kwargs)

    def to_primitive(self, context=None):
        return super(ModelList, self).to_primitive(context=context)['items']
