from schematics.exceptions import DataError
from schematics.models import Model
from ..errors import VaidationError


class BaseModel(Model):

    @classmethod
    def get_serialized_name(cls, attribute):
        field = cls._schema.fields.get(attribute)

        if field is None:
            raise ValueError('No attribute {} for {}', attribute, cls.__name__)

        return attribute if field.serialized_name is None else field.serialized_name

    def __init__(self, **kwargs):
        super(BaseModel, self).__init__(raw_data=kwargs, strict=False, validate=True)

    def validate(self):
        try:
            super(BaseModel, self).validate()
        except DataError as e:
            raise VaidationError(e.to_primitive())
