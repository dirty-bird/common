import jwt
from datetime import datetime, timedelta
from dateutil import parser


class TokenService:

    def __init__(self, secret):
        self._secret = secret

    def encode(self, expiration_date=None, never_expires=None, **kwargs):
        if len(kwargs.keys()) == 0:
            raise Exception()

        expiration_date = expiration_date if isinstance(expiration_date, datetime) else datetime.utcnow() + timedelta(minutes=60)

        if never_expires is not True:
            kwargs['__ed'] = str(expiration_date)

        return jwt.encode(kwargs, self._secret)

    def decode(self, token):
        decoded = jwt.decode(token, self._secret)

        if not decoded.get('__ed'):
            return decoded
        elif datetime.utcnow() < parser.parse(decoded['__ed']):
            del decoded['__ed']
            return decoded

        return None
